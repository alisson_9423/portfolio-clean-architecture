import { BrowserRouter } from "react-router-dom";
import { SwitchRoutes } from "@/presentation/components/SwitchRoutes";
import { ToastContainer } from "react-toastify";

import { GlobalStyle } from "@/presentation/styles/global";

import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import "react-toastify/dist/ReactToastify.css";
import "@/presentation/styles/fonts.css";

export function App() {
    return (
        <BrowserRouter>
            <GlobalStyle />
            <SwitchRoutes />
            <ToastContainer />
        </BrowserRouter>
    );
}
