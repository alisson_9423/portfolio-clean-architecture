import { MusicModel } from "@/domain/models";

export interface Music {
    loadAllMusic(search: string): Promise<MusicModel[]>;
}
