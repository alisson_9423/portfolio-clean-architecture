import { HttpStatusCode } from "@/data/protocols/http";

export class UnexpectedError extends Error {
    status: number;
    constructor() {
        super("Algo de errado aconteceu. Tente novamente em breve");
        this.name = "UnexpectedError";
        this.status = HttpStatusCode.serverError;
    }
}
