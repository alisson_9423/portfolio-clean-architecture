import { HttpStatusCode } from "@/data/protocols/http";

export class InvalidCredentialsErros extends Error {
    status: HttpStatusCode;
    constructor() {
        super("Credencias inválidas");
        this.name = "InvalidCredentialsErros";
        this.status = HttpStatusCode.unathorized;
    }
}
