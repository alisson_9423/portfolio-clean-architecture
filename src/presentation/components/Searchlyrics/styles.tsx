import styled from "styled-components";
import { Container as Button } from "@/presentation/components/Button";

export const Container = styled.div`
    background-image: ${({ theme }) =>
        !theme.isDark
            ? "url('https://images.unsplash.com/photo-1506157786151-b8491531f063?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80')"
            : "url('https://blog.emania.com.br/wp-content/uploads/2015/10/thumb-04.jpg')"};
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center center;
    color: ${({ theme }) => theme.colors.white};
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding: 100px 0;
    position: relative;
    transition: all 0.5s;
    z-index: 20;
    h1 {
        text-align: center;
        margin-bottom: 16px;
    }
    &::after {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        background-color: rgba(0, 0, 0, 0.6);
        z-index: 1;
    }

    .form-header {
        position: relative;
        z-index: 30;
        form {
            position: relative;
            width: 500px;
            max-width: 100%;
            margin: 0 auto;
            input {
                height: 48px;
                border: none;
            }

            ${Button} {
                position: absolute;
                top: 4px;
                right: 2px;
                background-color: ${({ theme }) => theme.colors.primary};
                border: 0;
                border-radius: 50px;
                color: ${({ theme }) => theme.colors.white};
                font-size: 16px;
                cursor: pointer;
                width: 110px;
                font-weight: 400;
                transition: all 0.5s;
                &:active {
                    transform: scale(0.95);
                }
            }
        }
    }

    @media (max-width: 480px) {
        width: 100%;
        .form-header {
            width: 90%;
            form {
                width: 100%;
            }
        }
    }
`;
