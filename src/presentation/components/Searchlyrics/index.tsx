import { FormSearchlyrics } from "@/presentation/components/FormSearchlyrics";
import { useTranslation } from "@/main/contexts/Localization";
import { Container } from "./styles";
export function Searchlyrics() {
    const { t } = useTranslation();

    return (
        <Container>
            <div className="form-header">
                <h1>{t("Busca letras")}</h1>
                <FormSearchlyrics />
            </div>
        </Container>
    );
}
