import { useHome, TabId } from "@/main/hooks/detailAction";
import { useTranslation } from "@/main/contexts/Localization";
import { Text } from "alisson-application";
import { Container } from "./styles";
export function Tabs() {
    const { tabs, updateTab } = useHome();
    const { t } = useTranslation();
    return (
        <Container>
            {tabs.map((tab) => {
                return (
                    <button
                        key={tab.id}
                        data-testid={`tab-${tab.className}`}
                        className={`${tab.active ? "active" : ""}`}
                        onClick={() => updateTab(tab.title as TabId)}
                    >
                        <Text as="h5">{t(tab.title)}</Text>
                    </button>
                );
            })}
        </Container>
    );
}
