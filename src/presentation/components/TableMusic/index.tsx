import { GridColumns, GridRenderCellParams } from "@mui/x-data-grid-pro";
import Marquee from "react-fast-marquee";
import Avatar from "@mui/material/Avatar";
import { Flex, Text } from "alisson-application";
import Table from "@/presentation/components/Table";
import { useMusic } from "@/presentation/components/Content/hooks/music";
import { useTranslation } from "@/main/contexts/Localization";

export function TableMusic() {
    const { musics, activeTr, setActiveTr, setPlayIndex } = useMusic();
    const { t } = useTranslation();
    const width = window.screen.width;

    if (!musics.length) {
        return <></>;
    }

    const rows = musics.map((music) => {
        return {
            id: music.id,
            title: music.title,
            artist: music.artist.name,
            album: music.album,
        };
    });

    const columns: GridColumns = [
        {
            field: "id",
            headerName: "Id",
            hide: true,
        },
        {
            field: "title",
            headerName: t("Titulo"),
            renderCell: ({ row }: GridRenderCellParams) => {
                if (row.id !== activeTr) {
                    return <Text>{row.title}</Text>;
                }

                return (
                    <Marquee gradient={false} speed={50}>
                        <Text>{row.title}</Text>
                    </Marquee>
                );
            },
            width: width < 480 ? 180 : 215,
        },
        {
            field: "artist",
            headerName: t("Artista"),
            renderCell: ({ row }: GridRenderCellParams) => {
                if (row.id !== activeTr) {
                    return <Text>{row.artist}</Text>;
                }

                return (
                    <Marquee gradient={false} speed={50}>
                        <Text>{row.artist}</Text>
                    </Marquee>
                );
            },
            width: width < 480 ? 180 : 215,
        },
        {
            field: "album",
            headerName: t("Album"),
            renderCell: ({ row }: GridRenderCellParams) => {
                return (
                    <Flex alignItems="center">
                        <Avatar
                            src={row.album.cover}
                            alt={row.title}
                            sx={{ width: 56, height: 56 }}
                        />
                        <Text ml="16px">{row.album.title}</Text>
                    </Flex>
                );
            },
            width: width < 480 ? 500 : 215,
        },
    ];

    return (
        <Table
            rows={rows}
            header={false}
            columns={columns}
            nameCSV="Music"
            onRowClick={(values) => {
                const getPosMusic = musics.findIndex(
                    (music) => music.id === values.id
                );
                setActiveTr(musics[getPosMusic].id);
                setPlayIndex(getPosMusic);
            }}
        />
    );
}
