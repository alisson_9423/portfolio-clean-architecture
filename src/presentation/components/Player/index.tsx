import { useEffect } from "react";
import { useTheme } from "@/main/contexts/ThemeContext";

import ReactJkMusicPlayer, {
    ReactJkMusicPlayerAudioListProps,
} from "react-jinke-music-player";

import { useMusic } from "@/presentation/components/Content/hooks/music";

import "react-jinke-music-player/lib/styles/index.less";
import "./theme.css";

interface PlayerProps {
    audioList: ReactJkMusicPlayerAudioListProps[];
}

export function Player(props: PlayerProps) {
    const { audioList } = props;
    const { setActiveTr, playIndex, setPlayIndex, musics } = useMusic();
    const { currentTheme, lightTheme, darkTheme } = useTheme();

    if (!audioList.length) return <></>;

    useEffect(() => {
        setPlayIndex(0);
        setActiveTr(musics[0].id);
    }, []);

    return (
        <ReactJkMusicPlayer
            glassBg
            audioLists={audioList}
            autoPlay={true}
            showPlayMode={false}
            onAudioEnded={() => {
                if (playIndex < 14) {
                    setPlayIndex(playIndex + 1);
                } else {
                    setPlayIndex(0);
                }
            }}
            theme={currentTheme.isDark ? "dark" : "light"}
            playIndex={playIndex}
            playModeShowTime={1000}
            mode="full"
            onThemeChange={() => {
                currentTheme.isDark ? lightTheme() : darkTheme();
            }}
            onPlayIndexChange={(index: number) => {
                setPlayIndex(index);
                setActiveTr(musics[index].id);
            }}
        />
    );
}
