import { Formik, Field, ErrorMessage, Form, FormikProps } from "formik";
import { Container as Button } from "@/presentation/components/Button";
import { initialValues, validation, MyFormValues } from "./config";

import { useTranslation } from "@/main/contexts/Localization";
import { useMusic } from "@/presentation/components/Content/hooks/music";
import { useRef } from "react";

export function FormSearchlyrics() {
    const { t } = useTranslation();
    const { fetchData } = useMusic();
    const ref = useRef<HTMLDivElement>(null);

    return (
        <Formik
            validateOnMount
            initialValues={initialValues}
            validationSchema={validation}
            onSubmit={async (values, actions) => {
                await fetchData(values.search);
                actions.resetForm();
                if (ref.current) {
                    ref.current.querySelector("input")?.blur();
                }
            }}
        >
            {(props: FormikProps<MyFormValues>) => {
                return (
                    <Form>
                        <div className="container-input" ref={ref}>
                            <Field
                                id="search"
                                type="text"
                                placeholder={t(
                                    "Insira o nome do artista ou da música..."
                                )}
                                name="search"
                            />
                            <ErrorMessage component="span" name="search" />
                        </div>

                        <Button
                            disabled={!props.isValid}
                            isLoading={props.isSubmitting}
                            type="submit"
                        >
                            {t("Buscar")}
                        </Button>
                    </Form>
                );
            }}
        </Formik>
    );
}
