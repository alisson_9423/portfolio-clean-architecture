import { Router } from "react-router-dom";
import Faker from "faker";
import { createMemoryHistory } from "history";
import { cleanup, render } from "@testing-library/react";
import { UseApiProvider } from "@/main/contexts/apiContext";
import { PageHome } from "@/main/pages";
import PrivateRoute from ".";
import { StorageLocation } from "@/infra/cache/storageLocation";

type SutTypes = {
    history: ReturnType<typeof createMemoryHistory>;
};

const makeSut = (path = "/acao"): SutTypes => {
    const history = createMemoryHistory({ initialEntries: ["/"] });
    const storageLocation = new StorageLocation();
    render(
        <Router history={history}>
            <UseApiProvider storageLocation={storageLocation}>
                <PrivateRoute
                    path={path}
                    component={PageHome}
                    isPrivate={true}
                />
            </UseApiProvider>
        </Router>
    );
    return {
        history,
    };
};

describe("Private Routes", () => {
    afterEach(cleanup);
    test("should redirect to / when no route exists", () => {
        const { history } = makeSut(Faker.random.word());
        expect(history.location.pathname).toBe("/");
    });

    test("should route is private but without the token redirect /", () => {
        const { history } = makeSut(Faker.random.word());
        expect(history.location.pathname).toBe("/");
    });
});
