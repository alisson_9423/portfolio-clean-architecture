import { UseMusicProvider } from "./hooks/music";

import { Music } from "@/presentation/components/Music";
import { useHome } from "@/main/hooks/detailAction";
import { useTranslation } from "@/main/contexts/Localization";
import { Container } from "./styles";

export function Content() {
    const { tabs } = useHome();
    const { t } = useTranslation();

    return (
        <Container>
            <div className={`container-tab ${tabs[0].active ? "active" : ""}`}>
                {tabs[0].viewer && (
                    <UseMusicProvider>
                        <Music />
                    </UseMusicProvider>
                )}
            </div>
            <div className={`container-tab ${tabs[1].active ? "active" : ""}`}>
                {tabs[1].viewer && <h1>{t("Em desenvolvimento")}</h1>}
            </div>
        </Container>
    );
}
