import React, { createContext, useContext, useState } from "react";
import { ReactJkMusicPlayerAudioListProps } from "react-jinke-music-player";
import { useHome } from "@/main/hooks/detailAction";
import { MusicModel } from "@/domain/models";

interface UseMusicProps {
    children: React.ReactNode | React.ReactNode[];
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface UseStateAction {
    loading: boolean;
    musics: MusicModel[];
    audioList: ReactJkMusicPlayerAudioListProps[];

    activeTr: number;
    setActiveTr: (value: number) => void;

    playIndex: number;
    setPlayIndex: (value: number) => void;

    fetchData(music: string): Promise<void>;
}

const Context = createContext<UseStateAction>({} as UseStateAction);

export function UseMusicProvider(props: UseMusicProps) {
    const { children } = props;
    const { home } = useHome();
    const [loading, setLoading] = useState(false);
    const [activeTr, setActiveTr] = useState<number>(0);
    const [playIndex, setPlayIndex] = useState<number>(0);
    const [musics, setMusics] = useState<MusicModel[]>([]);
    const [audioList, setAudioList] = useState<
        ReactJkMusicPlayerAudioListProps[]
    >([]);

    async function fetchData(music: string) {
        setLoading(true);
        try {
            const response = await home.loadAllMusic(music);

            setMusics(response);
            setActiveTr(response[0].id);
            setAudioList(home.formatMusicPlayer(response));
            setLoading(false);
        } catch (error) {
            setLoading(false);
        }
    }

    return (
        <Context.Provider
            value={{
                fetchData,
                musics,
                audioList,
                activeTr,
                setActiveTr,
                playIndex,
                setPlayIndex,
                loading,
            }}
        >
            {children}
        </Context.Provider>
    );
}

export function useMusic() {
    const context = useContext(Context);
    return context;
}
