import styled from "styled-components";

export const Container = styled.div`
    height: 218px;
    width: 390px;

    border: 1px solid ${(props) => props.theme.colors.secondarydark};
    border-radius: 10px;
    padding: 5px;

    .container-title {
        display: flex;
        align-items: center;
        margin-left: 12px;
        margin-bottom: 16px;
        margin-top: 16px;
        p {
            text-transform: unset;
        }
    }
`;
