import { Tabs } from "@/presentation/components/Tabs";
import { Content } from "@/presentation/components/Content";
import { Container } from "./styles";
import { When } from "@/presentation/components/when";
import { MUISpeedDial } from "@/presentation/components/SpeedDial";
import { MUISpeedDialLanguage } from "@/presentation/components/SpeedDialLanguage";

export function InitialPage() {
    const width = window.screen.width;

    return (
        <Container>
            <When expr={width > 480}>
                <Tabs />
            </When>

            <When expr={width <= 480}>
                <MUISpeedDial />
            </When>

            <MUISpeedDialLanguage />

            <Content />
        </Container>
    );
}
