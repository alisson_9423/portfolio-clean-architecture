import styled from "styled-components";

export const Container = styled.div`
    min-height: calc(100vh - 76px);
    padding-bottom: 50px;

    @media (max-width: 480px) {
        min-height: 100vh;
    }
`;
