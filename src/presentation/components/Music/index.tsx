import { useMusic } from "@/presentation/components/Content/hooks/music";
import { Searchlyrics } from "@/presentation/components/Searchlyrics";
import { TableMusic } from "@/presentation/components/TableMusic";
import { Player } from "@/presentation/components/Player";
import { Container } from "./styles";
import { Loader } from "../Loader";
import { When } from "@/presentation/components/when";
export function Music() {
    const { audioList, loading } = useMusic();

    return (
        <Container>
            <Searchlyrics />
            <div className="container">
                <When expr={loading}>
                    <Loader />
                </When>

                <When expr={!loading}>
                    <>
                        <TableMusic />
                        <Player audioList={audioList} />
                    </>
                </When>
            </div>
        </Container>
    );
}
