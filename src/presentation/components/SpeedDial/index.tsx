import SpeedDial from "@mui/material/SpeedDial";
import SpeedDialIcon from "@mui/material/SpeedDialIcon";
import SpeedDialAction from "@mui/material/SpeedDialAction";
import HeadsetIcon from "@mui/icons-material/Headset";
import MovieFilterIcon from "@mui/icons-material/MovieFilter";

import { TabId, useHome } from "@/main/hooks/detailAction";

const actions = [
    { icon: <HeadsetIcon />, name: "Música" },
    { icon: <MovieFilterIcon />, name: "Filmes" },
];

export function MUISpeedDial() {
    const { updateTab } = useHome();

    return (
        <SpeedDial
            ariaLabel="SpeedDial basic example"
            sx={{ position: "fixed", bottom: 12, right: 26 }}
            icon={<SpeedDialIcon />}
        >
            {actions.map((action) => (
                <SpeedDialAction
                    key={action.name}
                    icon={action.icon}
                    tooltipTitle={action.name}
                    onClick={() => updateTab(action.name as TabId)}
                />
            ))}
        </SpeedDial>
    );
}
