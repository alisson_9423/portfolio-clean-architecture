import styled from "styled-components";

export const Container = styled.div`
    position: fixed;
    bottom: 16px;
    right: 16px;
`;
