import SpeedDial from "@mui/material/SpeedDial";
import TranslateIcon from "@mui/icons-material/Translate";
import SpeedDialAction from "@mui/material/SpeedDialAction";
import Avatar from "@mui/material/Avatar";
import { Language, useTranslation } from "@/main/contexts/Localization";
import { languageList } from "@/main/contexts/Localization/config/languages";

const actions = [
    {
        icon: (
            <Avatar src="https://static.vecteezy.com/ti/vetor-gratis/p3/2431840-ilustracaoial-da-bandeira-brasil-gratis-vetor.jpg" />
        ),
        name: "pt-BR",
    },
    {
        icon: (
            <Avatar src="https://images2.alphacoders.com/476/thumbbig-476409.webp" />
        ),
        name: "en-US",
    },
];

export function MUISpeedDialLanguage() {
    const { setLanguage } = useTranslation();
    return (
        <SpeedDial
            ariaLabel="SpeedDial basic example"
            sx={{ position: "fixed", bottom: 12, left: 26 }}
            icon={<TranslateIcon />}
        >
            {actions.map((action) => (
                <SpeedDialAction
                    key={action.name}
                    icon={action.icon}
                    tooltipTitle={action.name}
                    onClick={() =>
                        setLanguage(
                            languageList.find(
                                (lenguage) => lenguage.locale === action.name
                            ) as Language
                        )
                    }
                />
            ))}
        </SpeedDial>
    );
}
