import { ValidationDate } from ".";

type SutType = {
    sut: ValidationDate;
};

const makeSut = (date: string | Date): SutType => {
    return {
        sut: new ValidationDate(date),
    };
};

describe("ValidationDate", () => {
    test("should returns a valid date passing with parameter new Date", () => {
        const date = new Date();
        const dia = date.getDate().toString().padStart(2, "0");
        const mes = (date.getUTCMonth() + 1).toString().padStart(2, "0");
        const ano = date.getUTCFullYear().toString().padStart(2, "0");

        const { sut } = makeSut(date);
        expect(sut.user()).toBe(`${dia}.${mes}.${ano}`);
    });

    test("should returns a valid date passing with parameter string", () => {
        const { sut } = makeSut("2023-03-07 00:00:00");
        expect(sut.user()).toBe("07.03.2023");
    });

    test('should returns a valid date passing with new Date("2023-03-07 00:00:00") ', () => {
        const { sut } = makeSut(new Date("2023-03-07 00:00:00"));
        expect(sut.user()).toBe("07.03.2023");
    });

    test("should returns a erro passing string invalid with new Date(teste)", () => {
        const { sut } = makeSut(new Date("teste"));
        expect(sut.user()).toBe("Invalid Date");
    });

    test("should returns a erro passing string invalid with new Date(teste)", () => {
        const { sut } = makeSut("teste");
        expect(sut.user()).toBe(`teste`);
    });
});
