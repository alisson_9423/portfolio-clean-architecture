import { Money } from "./money";

const makeSut = new Money();

describe("Money", () => {
    test("should return a string with BRL currency", () => {
        const value = "20.50";
        const result = makeSut.convertToBRL(value);
        expect(result).toBe("R$ 20,50");
    });

    test("converte corretamente para BRL sem centavos", () => {
        const valor = "100";
        const valorBRL = makeSut.convertToBRL(valor);
        expect(valorBRL).toBe("R$ 100,00");
    });

    test("converte corretamente para BRL com vírgula", () => {
        const valor = "1500.75";
        const valorBRL = makeSut.convertToBRL(valor);
        expect(valorBRL).toBe("R$ 1.500,75");
    });

    test("converte corretamente para BRL com ponto e vírgula", () => {
        const valor = "2000.00";
        const valorBRL = makeSut.convertToBRL(valor);
        expect(valorBRL).toBe("R$ 2.000,00");
    });
});
