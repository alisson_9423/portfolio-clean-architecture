import { Url } from "./generateValidUrl";

type SutTypes = {
    sut: Url;
};
const makeSut = (): SutTypes => {
    const sut = new Url();

    return {
        sut,
    };
};
describe("Url", () => {
    test("should simple test", () => {
        const { sut } = makeSut();
        expect(sut.slug("Hello World")).toBe("hello-world");
    });

    test("should text with special characters and accents", () => {
        const { sut } = makeSut();
        expect(sut.slug("Olá Mundo!")).toBe("ola-mundo");
    });

    test("should text with white spaces", () => {
        const { sut } = makeSut();
        expect(sut.slug("   Texto com espaços  ")).toBe("texto-com-espacos");
    });

    test("should text with underscores", () => {
        const { sut } = makeSut();
        expect(sut.slug("Texto_com_underscores")).toBe("texto-com-underscores");
    });

    test("should text with special characters", () => {
        const { sut } = makeSut();
        expect(sut.slug("Texto#com#caracteres#especiais")).toBe(
            "texto-com-caracteres-especiais"
        );
    });

    test("should text with repeated spaces", () => {
        const { sut } = makeSut();
        expect(sut.slug("Texto  Com  Espaços  Repetidos")).toBe(
            "texto-com-espacos-repetidos"
        );
    });
    test("should text with repeated hyphens", () => {
        const { sut } = makeSut();
        expect(sut.slug("Texto---Com---Hifens---Repetidos")).toBe(
            "texto-com-hifens-repetidos"
        );
    });
    test("should Text with hyphens at the beginning and end---", () => {
        const { sut } = makeSut();
        expect(sut.slug("Texto com hífens no início e no final---")).toBe(
            "texto-com-hifens-no-inicio-e-no-final"
        );
    });
});
