import { InitialPage } from "@/presentation/components/InitialPage";
import { UseHomeProvider } from "@/main/hooks/detailAction";
import { Home } from "@/data/useCases/home";

export function PageHome() {
    const home = new Home();

    return (
        <UseHomeProvider home={home}>
            <InitialPage />
        </UseHomeProvider>
    );
}
