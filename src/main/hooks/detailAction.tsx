import React, { createContext, useContext, useState } from "react";
import { Home } from "@/data/useCases/home";

interface UseHomeProps {
    children: React.ReactNode | React.ReactNode[];
    home: Home;
}

export interface UseStateHome {
    tabs: Tabs[];
    setTabs: (tabs: Tabs[]) => void;
    updateTab(id: TabId): void;
    home: Home;
}

export interface Tabs {
    id: number;
    title: string;
    className: string;
    active: boolean;
    viewer: boolean;
    permission: boolean;
}

export type TabId = "Música" | "tab2" | "tab3" | "tab4" | "tab5" | "tab6";

const itens = [
    {
        id: 1,
        title: "Música",
        className: "musica",
        active: true,
        viewer: true,
        permission: true,
    },
    {
        id: 2,
        title: "Filmes",
        className: "tab2",
        active: false,
        viewer: false,
        permission: false,
    },
];

export interface historyProps {
    id: number;
    client: string;

    avaliable: number;
    revoked: number;
    rewarded: number;
    total: number;
}

const Context = createContext<UseStateHome>({} as UseStateHome);

export function UseHomeProvider(props: UseHomeProps) {
    const { children, ...rest } = props;

    const [tabs, setTabs] = useState<Tabs[]>(itens);

    function updateTab(id: TabId) {
        const update = tabs.map((tab) => {
            if (tab.title === id) {
                return {
                    ...tab,
                    active: true,
                    viewer: true,
                    permission: true,
                };
            }

            return {
                ...tab,
                active: false,
                viewer: tab.viewer,
                permission: false,
            };
        });

        setTabs(update);
    }

    return (
        <Context.Provider
            value={{
                tabs,
                setTabs,
                updateTab,
                ...rest,
            }}
        >
            {children}
        </Context.Provider>
    );
}

export function useHome() {
    const context = useContext(Context);
    return context;
}
