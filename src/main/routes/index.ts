import { PageHome } from "@/main/pages";

import { routesProps } from "./types";

export const routes: routesProps[] = [
    {
        path: "/",
        exact: true,
        component: PageHome,
        isPrivate: false,
    },
];
