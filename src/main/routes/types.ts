export interface routesProps {
    path: string;
    exact?: boolean | undefined;
    component: React.ComponentType<unknown>;
    isPrivate: boolean;
    permission?: string;
}
