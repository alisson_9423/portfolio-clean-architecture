import React, { useState, useContext } from "react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { ptBR } from "@mui/x-date-pickers";
import { ptBR as coreptBr } from "@mui/material/locale";
import { ThemeProvider as SCThemeProvider } from "styled-components";
import { themeColors, themeColorsDark } from "@/presentation/styles/theme";
import { Theme } from "alisson-application";

interface SCThemeProviderProps {
    children: React.ReactNode | React.ReactNode[];
}
interface ThemeProviderProps {
    currentTheme: Theme;
    lightTheme: () => void;
    darkTheme: () => void;
}

const ThemeContext = React.createContext<ThemeProviderProps>(
    {} as ThemeProviderProps
);

const ThemeContextProvider = ({ children }: SCThemeProviderProps) => {
    const [currentTheme, setCurrentTheme] = useState<Theme>(themeColors);
    function lightTheme() {
        setCurrentTheme(themeColors);
    }

    function darkTheme() {
        setCurrentTheme(themeColorsDark);
    }

    const theme = createTheme(
        {
            palette: {
                primary: { main: currentTheme.colors.primary },
            },
        },
        ptBR, // x-date-pickers translations
        coreptBr // core translations
    );

    return (
        <ThemeContext.Provider value={{ lightTheme, darkTheme, currentTheme }}>
            <SCThemeProvider theme={currentTheme}>
                <ThemeProvider theme={theme}>{children}</ThemeProvider>
            </SCThemeProvider>
        </ThemeContext.Provider>
    );
};

export function useTheme() {
    const context = useContext(ThemeContext);
    return context;
}

export { ThemeContext, ThemeContextProvider };
