import { UseApiProvider } from "@/main/contexts/apiContext";
import { ThemeContextProvider } from "@/main/contexts/ThemeContext";
import { UseModalProvider } from "@/main/contexts/modal";
import { LanguageProvider } from "@/main/contexts/Localization/";
import { StorageLocation } from "@/infra/cache/storageLocation";

interface ProvidersProps {
    children: JSX.Element | JSX.Element[] | string | string[];
}

export default function Providers({ children }: ProvidersProps) {
    const localStoraged = new StorageLocation();

    return (
        <UseApiProvider storageLocation={localStoraged}>
            <ThemeContextProvider>
                <UseModalProvider>
                    <LanguageProvider>{children}</LanguageProvider>
                </UseModalProvider>
            </ThemeContextProvider>
        </UseApiProvider>
    );
}
