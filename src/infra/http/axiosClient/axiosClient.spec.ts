import { AxiosHttpClient } from "./axiosClient";
import { mockAxios } from "@/infra/test";
import Faker from "faker";
import axios from "axios";
import { mockHttpRequest } from "@/data/test";
import { StorageLocation } from "@/infra/cache/storageLocation";
import "jest-localstorage-mock";

jest.mock("axios");

type SutTypes = {
    sut: AxiosHttpClient;
    mockedAxios: jest.Mocked<typeof axios>;
    mockStorage: StorageLocation;
};

const makeSut = (): SutTypes => {
    const sut = new AxiosHttpClient("teste");
    const mockedAxios = mockAxios();
    const mockStorage = new StorageLocation();

    return {
        sut,
        mockedAxios,
        mockStorage,
    };
};

describe("AxiosHttpClient", () => {
    beforeEach(() => {
        localStorage.clear();
    });

    describe("request", () => {
        test("should call axios with correct values", async () => {
            const request = mockHttpRequest();
            const { sut } = makeSut();
            const axiosInstanceMock = jest.spyOn(sut, "request");
            await sut.request(request);

            expect(axiosInstanceMock).toHaveBeenCalledWith({
                url: request.url,
                body: request.body,
                headers: request.headers,
                method: request.method,
            });
        });

        test("should return  correct statusCode response on axios", async () => {
            const { sut } = makeSut();
            const mockedAxios = jest.spyOn(sut, "request");
            const response = await sut.request(mockHttpRequest());
            const axiosResponse = await mockedAxios.mock.results[0].value;

            expect(response).toEqual({
                statusCode: axiosResponse?.status,
                body: axiosResponse?.data,
            });
        });

        // test("should return correct erro axios", () => {
        //     const { sut } = makeSut();
        //     const request = mockHttpRequest();
        //     const mockedAxios = jest
        //         .spyOn(sut, "request")
        //         .mockRejectedValueOnce({
        //             response: mockHttpResponse(),
        //         });
        //     const response = sut.request(request);

        //     expect(response).toBe(mockedAxios.mock.results[0].value);
        // });
    });

    describe("authenticated", () => {
        test("should add authorization token to headers if token exists", async () => {
            const { sut, mockStorage } = makeSut();
            const token = { access_token: "my_token" };
            await mockStorage.set("access_token", token);

            const headers = {
                "Content-Type": "application/json",
            };

            const data = {
                url: "/api/users",
                method: Faker.random.arrayElement([
                    "get",
                    "post",
                    "put",
                    "delete",
                ]),
                headers: headers,
            };

            const result = sut.authenticated(data);

            expect(result.Authorization).toBe(`Bearer ${token.access_token}`);
        });

        test("should not add authorization token to headers if token does not exist", () => {
            const { sut } = makeSut();

            const headers = {
                "Content-Type": "application/json",
            };

            const data = {
                url: "/api/users",
                method: "GET",
                headers: headers,
            };

            const result = sut.authenticated(data);

            expect(result.headers).toEqual(headers);
        });
    });
});
