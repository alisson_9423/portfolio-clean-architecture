import Faker from "faker";
import { StorageLocation } from "./storageLocation";
import "jest-localstorage-mock";

const makeSut = (): StorageLocation => new StorageLocation();

describe("LocalStorageAdapter", () => {
    beforeEach(() => {
        localStorage.clear();
    });

    test("should call localStorage with correct values", async () => {
        const sut = makeSut();
        const key = Faker.random.word();
        const value = Faker.random.objectElement<{}>();

        await sut.set(key, value);
        expect(localStorage.setItem).toHaveBeenCalledWith(
            key,
            JSON.stringify(value)
        );
    });

    test("should call LocalStorage.set remove item id values is null", () => {
        const sut = makeSut();
        const key = Faker.database.column();

        sut.set(key, undefined);
        expect(localStorage.removeItem).toHaveBeenCalledWith(key);
    });

    test("should call LocalStorage,get with correct values", () => {
        const sut = makeSut();
        const key = Faker.database.column();
        const value = Faker.random.objectElement<{}>();
        const getItem = jest
            .spyOn(localStorage, "getItem")
            .mockReturnValueOnce(JSON.stringify(value));
        const obj = sut.get(key);
        expect(obj).toEqual(value);
        expect(getItem).toHaveBeenLastCalledWith(key);
    });

    test("should call LocalStorage.clear remove localStorage", async () => {
        const sut = makeSut();
        const key = Faker.database.column();
        const value = Faker.random.objectElement<{}>();

        await sut.set(key, value);
        expect(localStorage.setItem).toHaveBeenCalledWith(
            key,
            JSON.stringify(value)
        );

        const result = sut.clear();
        expect(result).toBe(null);
    });
});
