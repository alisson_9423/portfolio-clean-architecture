import axios from "axios";
import Faker from "faker";

export const mockHttpResponse = (): any => ({
    data: Faker.random.objectElement(),
    status: Faker.random.number(),
});

export const mockAxios = (): jest.Mocked<typeof axios> => {
    const mockedAxios = axios as jest.Mocked<typeof axios>;

    mockedAxios.request.mockClear().mockResolvedValue(mockHttpResponse());

    return mockedAxios;
};
