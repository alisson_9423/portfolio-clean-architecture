import { RemoteMusicList } from "@/data/useCases/music/remoteMusicList";

import { AxiosHttpClient } from "@/infra/http/axiosClient/axiosClient";
import { Mixin } from "ts-mixer";

const axios = new AxiosHttpClient("https://api.lyrics.ovh");

export class Home extends Mixin(RemoteMusicList) {
    constructor() {
        super(axios);
    }
}
