import { ReactJkMusicPlayerAudioListProps } from "react-jinke-music-player";
import { HttpClient, HttpStatusCode } from "@/data/protocols/http";
import { UnexpectedError } from "@/domain/errors";
import { AccessDeniedError } from "@/domain/errors/accessDeniedError";
import { MusicDataModel, MusicModel } from "@/domain/models";

import { Music } from "@/domain/useCases";

export class RemoteMusicList implements Music {
    constructor(
        protected readonly httpGetClient: HttpClient<MusicDataModel>,
        protected readonly urlLoadMusic: string = "/suggest"
    ) {}

    async loadAllMusic(params: string): Promise<MusicModel[]> {
        const response = await this.httpGetClient.request({
            url: `${this.urlLoadMusic}/${params}`,
            method: "get",
        });

        switch (response.statusCode) {
            case HttpStatusCode.ok:
                if (response.body) {
                    return this.formatMusic(response.body.data);
                }
                return [] as MusicModel[];
            case HttpStatusCode.forbidden:
                throw new AccessDeniedError();
            case HttpStatusCode.noContent:
                return [] as MusicModel[];
            default:
                throw new UnexpectedError();
        }
    }

    protected formatMusic(musics: MusicModel[]): MusicModel[] {
        const update = musics.map((music) => {
            const { preview } = music;
            const posLastBar = preview.lastIndexOf("/");
            let baseUrl = preview.substring(0, posLastBar);

            const code = preview.substring(posLastBar + 1, preview.length);

            baseUrl = baseUrl.replace("http://", "https://");
            baseUrl = baseUrl.replace("cdn", "cdns");
            baseUrl = baseUrl.replace(".deezer.com", ".dzcdn.net");

            return { ...music, preview_deezer: baseUrl + "/" + code };
        });

        return update;
    }

    formatMusicPlayer(
        musics: MusicModel[]
    ): ReactJkMusicPlayerAudioListProps[] {
        const update = musics.map((music) => {
            return {
                name: music.title,
                singer: music.artist.name,
                cover: music.album.cover_big,
                musicSrc: music.preview_deezer,
            };
        });

        return update;
    }
}
