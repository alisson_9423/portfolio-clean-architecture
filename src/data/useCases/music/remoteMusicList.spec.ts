import { HttpClientSpy } from "@/data/test";
import Faker from "faker";
import { RemoteMusicList } from "./remoteMusicList";
import { HttpStatusCode } from "@/data/protocols/http";
import { AccessDeniedError } from "@/domain/errors/accessDeniedError";
import { UnexpectedError } from "@/domain/errors";
import { mockMusicModel } from "@/domain/test";

type SutType = {
    sut: RemoteMusicList;
    httpClientSpy: HttpClientSpy;
};

const makeSut = (url: string = Faker.internet.url()): SutType => {
    const httpClientSpy = new HttpClientSpy();
    const sut = new RemoteMusicList(httpClientSpy, url);

    return {
        sut,
        httpClientSpy,
    };
};

describe("RemoteMusicList", () => {
    test("should call with correct URL and method", async () => {
        const url = Faker.internet.url();
        const music = Faker.internet.url();
        const { sut, httpClientSpy } = makeSut(url);
        await sut.loadAllMusic(music);
        expect(httpClientSpy.method).toBe("get");
        expect(httpClientSpy.url).toBe(`${url}/${music}`);
    });

    test("should call throwsAccessdinedError if HttpGetClient return 403", async () => {
        const { httpClientSpy, sut } = makeSut();
        httpClientSpy.response = {
            statusCode: HttpStatusCode.forbidden,
        };

        const promise = sut.loadAllMusic(Faker.random.word());
        await expect(promise).rejects.toThrow(new AccessDeniedError());
    });

    test("should throws UnexpecError if HttpGetClient returns 404", async () => {
        const { httpClientSpy, sut } = makeSut();

        httpClientSpy.response = {
            statusCode: HttpStatusCode.notFound,
        };
        const promise = sut.loadAllMusic(Faker.random.word());
        await expect(promise).rejects.toThrow(new UnexpectedError());
    });

    test("should throws UnexpecError if HttpGetClient returns 500", async () => {
        const { sut, httpClientSpy } = makeSut();

        httpClientSpy.response = {
            statusCode: HttpStatusCode.serverError,
        };

        const promise = sut.loadAllMusic(Faker.random.word());
        await expect(promise).rejects.toThrow(new UnexpectedError());
    });

    test("should return a list of ActionDetails if HttpGetClient return 200", async () => {
        const { sut, httpClientSpy } = makeSut();

        const resultMock = mockMusicModel();
        httpClientSpy.response = {
            statusCode: HttpStatusCode.ok,
            body: resultMock,
        };

        const response = await sut.loadAllMusic(Faker.random.word());

        expect(response).toEqual(resultMock);
    });
});
