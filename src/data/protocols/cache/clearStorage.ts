export interface ClearStorage {
    clear: () => void;
}
