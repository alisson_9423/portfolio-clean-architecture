import Faker from "faker";
import {
    HttpClient,
    HttpRequest,
    HttpResponse,
    HttpStatusCode,
} from "@/data/protocols/http";

export const mockHttpRequest = (): HttpRequest => ({
    url: Faker.random.word(),
    method: Faker.random.arrayElement(["get", "post", "put", "delete"]),
    body: Faker.random.objectElement(),
    headers: Faker.random.objectElement(),
});

export class HttpClientSpy<R = any> implements HttpClient<R> {
    url?: string;
    body?: unknown;
    method?: string;
    headers?: any;
    response: HttpResponse<R> = {
        statusCode: HttpStatusCode.ok,
    };

    async request(data: HttpRequest): Promise<HttpResponse<R>> {
        this.url = data.url;
        this.body = data.body;
        this.method = data.method;
        this.headers = data.headers;

        return this.response;
    }
}
