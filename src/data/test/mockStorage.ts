import Faker from "faker";
import { SetStorage } from "@/data/protocols/cache/setStorage";
import { GetStorage } from "../protocols/cache";

export class SetStorageSpy implements SetStorage {
    key: string | undefined;
    value: unknown;

    set(key: string, value: any): Promise<void> {
        this.key = key;
        this.value = value;
        return Promise.resolve();
    }
}

export class GetStorageSpy implements GetStorage {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    key!: string;
    value: any = Faker.random.objectElement();
    get(key: string): any {
        this.key = key;
        return this.value;
    }
}
