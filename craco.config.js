const path = require("path");
module.exports = {
    webpack: {
        alias: {
            "@": path.resolve(__dirname, "src"),
        },
    },
    jest: {
        babel: {
            addPresets: true /* (default value) */,
            addPlugins: true /* (default value) */,
        },
        configure: {
            preset: "ts-jest",
            testEnvironment: "node",
            verbose: true,
            resetMocks: false,
            roots: ["<rootDir>/src"],
            transformIgnorePatterns: ["<rootDir>/node_modules/"],
            collectCoverageFrom: ["<rootDir>/src/**/*.{ts,tsx}", "!**/*.d.ts"],
            coverageDirectory: "coverage",
            testEnvironment: "jsdom",
            moduleDirectories: ["node_modules", "src"],
            transform: {
                "^.+\\.(ts|tsx)$": "ts-jest",
            },
            moduleNameMapper: {
                "^@/(.*)$": "<rootDir>/src/$1",
                "^axios$": require.resolve("axios"),
            },
        },
    },
};
